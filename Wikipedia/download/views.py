from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
import wikipedia
import PyPDF2
from .forms import  WikiPageForm
from django.http import HttpResponse, HttpResponseNotFound
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
# Create your views here.


# Create your views here.

def wikisearch(request):
    form = WikiPageForm()
    if request.method == 'POST':
        form = WikiPageForm(request.POST)
        if form.is_valid():
            search_option = form.cleaned_data['search_option']
            complete_content = wikipedia.page(search_option)
            complete_content_list = complete_content.content.split('\n')
            buffer = io.BytesIO()
            p = canvas.Canvas(buffer)
            p.drawString(100,700,complete_content.content)
            p.showPage()
            p.save()
            buffer.seek(0)
            return FileResponse(buffer, as_attachment=True, filename=search_option)
    my_dict = {'form': form}
    return render(request, 'home_page.html', context=my_dict)